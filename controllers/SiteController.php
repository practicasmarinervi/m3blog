<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Mensajes;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    // vamos a controlar las reglas de acceso
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                
                'only' => ['enviar'],
                'rules' => [
                    [
                        'actions' => ['enviar'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        // cargamos la pagina de inicio
        return $this->render('index');
    }

    public function actionLogin()
    {
        // en caso de no estar logueado nos colocamos en la pagina de inicio
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        // en caso de intentar realizar un logueo
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            // si es correcto volvemos a la pagina anterior
            return $this->goBack();
        }
        
        // en caso de que el logueo no sea correcto no entramos
        return $this->render('login', [
            'model' => $model, //a la vista login le pasa el modelo del loginForm
        ]);
    }


    public function actionLogout()
    {
        // nos salimos de la sesion
        Yii::$app->user->logout();

        return $this->goHome(); //se va al controlador y accion por defecto
    }


    
    public function actionLeer()
    {
        $consulta=Mensajes::find();
        if(!Yii::$app->user->isGuest){
            $consulta->where(["autor"=>Yii::$app->user->identity->usuario]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $consulta,
        ]);

        return $this->render('leer', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionEnviar()
    {
        $model = new Mensajes();
        $model->autor=Yii::$app->user->identity->usuario;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }

        return $this->render('enviar', [
            'model' => $model,
        ]);
    }
}
