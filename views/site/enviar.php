<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model app\models\Mensajes */

$this->title = 'Enviar mensaje nuevo';
$this->params['breadcrumbs'][] = ['label' => 'Mensajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mensajes-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="mensajes-form">

<?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'texto')->textarea(['rows' => 6]) ?>
        <?=
        $form->field($model, 'codigo')->widget(Captcha::className(), [
            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
        ])
        ?>

        <div class="form-group">
            <?= Html::submitButton('Enviar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>





